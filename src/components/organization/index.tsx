import React from "react";
import { css } from "@emotion/core";
import styled from "@emotion/styled";
import { Wrapper } from "../../common/components/wrapper";
import { Link } from "../../common/components/link";

export interface IOrganizationWrapperProps {
  background: string;
  color: string;
}

const OrganizationWrapper = styled.div<IOrganizationWrapperProps>(
  ({ background, color }) => ({
    background,
    color,
  }),
);

const FlexWrapper = styled.div({
  display: "flex",
});

const InfoWrapper = styled.div({
  flexGrow: 2,
  display: "flex",
  justifyContent: "center",
  flexDirection: "column",
  padding: 16,
  minWidth: 250,
});

const HighlightWrapper = styled.div({
  flexGrow: 2,
  display: "flex",
  justifyContent: "center",
  flexDirection: "column",
  padding: 16,
});

const Duration = styled.h5({
  marginTop: 8,
});

interface IOrganizationProps {
  name: string;
  location: string;
  title: string;
  duration: string;
  backgroundStyle: string;
  colorStyle: string;
  url: string;
  highlights: string[];
}

export const Organization: React.SFC<IOrganizationProps> = ({
  name,
  location,
  title,
  duration,
  highlights,
  backgroundStyle,
  colorStyle,
  url,
}) => (
  <OrganizationWrapper background={backgroundStyle} color={colorStyle}>
    {url && (
      <Link
        to={url}
        cssStyle={{
          position: "absolute",
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
        }}
      />
    )}
    <Wrapper>
      <FlexWrapper>
        <InfoWrapper>
          <h1>{name}</h1>
          <h5>{location}</h5>
          <Duration>{duration}</Duration>
        </InfoWrapper>
        <HighlightWrapper>
          <h2>{title}</h2>
          <ul>
            {highlights.map((h, i) => (
              <li key={i}>{h}</li>
            ))}
          </ul>
        </HighlightWrapper>
      </FlexWrapper>
    </Wrapper>
  </OrganizationWrapper>
);

Organization.displayName = "Organization";
