import styled from "@emotion/styled";

export const Wrapper = styled.div({
  maxWidth: 1130,
  marginLeft: "auto",
  marginRight: "auto",
  paddingLeft: 16,
  paddingRight: 16,
});
