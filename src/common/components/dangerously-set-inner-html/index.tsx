import React from "react";
import { css } from "@emotion/core";

interface IDangerouslySetInnerHtmlProps {}

export const DangerouslySetInnerHtml: React.SFC<
  IDangerouslySetInnerHtmlProps
> = ({ children }) => (
  <div dangerouslySetInnerHTML={{ __html: children.toString() }} />
);

DangerouslySetInnerHtml.displayName = "DangerouslySetInnerHtml";
